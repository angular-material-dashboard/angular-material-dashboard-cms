'use strict';

angular.module('ngMaterialDashboardCms')

/**
 * @ngdoc controller
 * @name AmdContentNewCtrl
 * @description Mange content new
 */
.controller('AmdContentNewCtrl', function($scope, $cms, $navigator) {
    
	var ctrl = {
		savingContent : false
	};

	function cancel() {
		$navigator.openPage('/contents');
	}

	function add(config) {
		ctrl.savingContent = true;
		var data = config.model;
		if(typeof data.title === 'undefined'){
			data.title = data.name;
		}
		var promise = $cms.putContent(data);
        if(config.files[0]){
            promise = promise.then(function(content){
                var file = config.files[0].lfFile;
                return content.uploadValue(file);
            });
        }
		promise.then(function(obj) {
			$navigator.openPage('/content/' + obj.id);
		}, function(error) {
			alert('Fail to create content:' + error.data.message);
		})
		.finally(function(){
            ctrl.savingContent = false;
		});
	}

	$scope.cancel = cancel;
	$scope.add = add;
	$scope.ctrl = ctrl;
});
