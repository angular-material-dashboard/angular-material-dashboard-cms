'use strict';

describe('Controller AmdContentCtrl', function() {

	// load the controller's module
	beforeEach(module('ngMaterialDashboardCms'));

	var AmdContentCtrl;
	var scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope, _$usr_) {
		scope = $rootScope.$new();
		AmdContentCtrl = $controller('AmdContentCtrl', {
			$scope : scope,
			$usr : _$usr_
			// place here mocked dependencies
		});
	}));

	it('should be defined', function() {
		expect(angular.isDefined(AmdContentCtrl)).toBe(true);
	});
});
