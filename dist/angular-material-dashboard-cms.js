/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardCms', [ //
    'ngMaterialDashboard',//
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardCms')
/**
 * 
 */
.config(function($routeProvider) {
	$routeProvider //
	.when('/contents', {
		controller : 'AmdContentsCtrl',
		controllerAs: 'ctrl',
		templateUrl : 'views/amd-contents.html',
		navigate : true,
		groups : [ 'content-management' ],
		name : 'Contents',
		icon : 'image',
		protect: true
	}) //
	.when('/contents/new', {
		controller : 'AmdContentNewCtrl',
		templateUrl : 'views/amd-content-new.html',
		navigate : true,
		groups : [ 'content-management' ],
		name : 'New Content',
		icon : 'note_add',
		protect: true,
		// Integerate with dashboard
//	    $navigator.scopePath($scope)//
//		.add({
//			title: 'Contents',
//			active: function(){
//				$navigator.openPage('contents');
//			}
//		})//
//		.add({
//			title: 'Hidden navigator',
//			active: function(){
//				$navigator.openPage('contents/new');
//			}
//		});
	}) //
	.when('/content/:contentId', {
		controller : 'AmdContentCtrl',
		controllerAs: 'ctrl',
		templateUrl : 'views/amd-content.html',
		protect: true, 
//		// Integerate with dashboard
//		integerate: function (){
//		    return $navigator.scopePath($scope)
//		    .clear()//
//			.add({
//				title: 'Contents',
//				active: function(){
//					$navigator.openPage('contents');
//				}
//			})//
//			.add({
//				title: $scope.content.title,
//				active: function(){
//					$navigator.openPage('contents/'+$scope.content.id);
//				}
//			});
//		}
	})
	

    .when('/terms', {
        controller : 'AmdCmsTermsCtrl',
        controllerAs: 'ctrl',
        templateUrl : 'views/amd-terms.html',
        navigate : true,
        groups : [ 'content-management' ],
        name : 'Terms',
        icon : 'note_add',
        protect: true
    })
    .when('/terms/:termId', {
        controller : 'AmdCmsTermCtrl',
        controllerAs: 'ctrl',
        templateUrl : 'views/amd-term.html'
    })
    .when('/term-taxonomies', {
        controller : 'AmdCmsTermTaxonomiesCtrl',
        controllerAs: 'ctrl',
        templateUrl : 'views/amd-term-taxonomies.html',
        navigate : true,
        groups : [ 'content-management' ],
        name : 'Term taxonomis',
        icon : 'note_add',
        protect: true
    })
    .when('/term-taxonomies/:taxonomyId', {
        controller : 'AmdCmsTermTaxonomyCtrl',
        controllerAs: 'ctrl',
        templateUrl : 'views/amd-term-taxonomy.html'
    });
});
'use strict';

angular.module('ngMaterialDashboardCms')

/**
 * @ngdoc controller
 * @name AmdContentNewCtrl
 * @description Mange content new
 */
.controller('AmdContentNewCtrl', function($scope, $cms, $navigator) {
    
	var ctrl = {
		savingContent : false
	};

	function cancel() {
		$navigator.openPage('/contents');
	}

	function add(config) {
		ctrl.savingContent = true;
		var data = config.model;
		if(typeof data.title === 'undefined'){
			data.title = data.name;
		}
		var promise = $cms.putContent(data);
        if(config.files[0]){
            promise = promise.then(function(content){
                var file = config.files[0].lfFile;
                return content.uploadValue(file);
            });
        }
		promise.then(function(obj) {
			$navigator.openPage('/content/' + obj.id);
		}, function(error) {
			alert('Fail to create content:' + error.data.message);
		})
		.finally(function(){
            ctrl.savingContent = false;
		});
	}

	$scope.cancel = cancel;
	$scope.add = add;
	$scope.ctrl = ctrl;
});


angular.module('ngMaterialDashboardCms')

/**
 * @ngdoc function
 * @name ngMaterialDashboard.controller:ContentCtrl
 * @description # ContentCtrl Controller of the ngMaterialDashboard
 */
.controller('AmdContentCtrl', function($scope, $cms, $dispatcher, $window,
        $routeParams, $location, CmsContent, CmsContentMetadata, $resource, $clipboard) {
    'use strict';
    var graphqlQuery = '{id,name,title,description,status,creation_dtime,modif_dtime,downloads,file_name,file_size,media_type,mime_type,term_taxonomies{id,taxonomy,term{id,name}},metas{id,key,value}}';

    this.loadingContent = true;
    this.savingContent= false;
    this.items = [];
    this.metadata = [];
    this.edit = false;
    this.content = null;

    this.contentJob = false;
    this.termtaxonomyJob = false;
    this.metadataJob = false;


    function handlError(){
        alert('faile to load content');
    }

    /**
     * درخواست مورد نظر را از سیستم حذف می‌کند.
     * 
     * @param request
     * @returns
     */
    this.remove = function () {
        var ctrl = this;
        confirm('delete content ' + this.content.id +'?')//
        .then(function(){
            return ctrl.content.delete();//
        })//
        .then(function(){
            // TODO: maso, 1395: go to the model page
            $location.path('/contents');
        }, function(error){
            alert('fail to delete content:' + error.message);
        });
    };

    this.save = function(){
        var ctrl = this;
        this.savingContent = true;
        this.content.update()//
        .then(function(){
            ctrl.edit=false;
            ctrl.savingContent = false;
        }, function(){
            alert('An error is occured while updating content.');
            ctrl.savingContent = false;			
        });
    };

    this.addTermTaxonomy = function(){
        var ctrl = this;
        this.termtaxonomyJob = true;
        $resource.get('cms/term-taxonomies', {
            style:{
                title: 'Term taxonomy',
            },
            data: null
        })//
        .then(function(termTaxonomy){
            // replace with content.putTermTaxonomy()
            return termTaxonomy.putContent(ctrl.content);
        })
        .then(function(){
            // TODO: maso, 2019: flux fire content termtaxonomy added
            ctrl.loadContent();
        })
        .finally(function(){
            ctrl.termtaxonomyJob = false;
        });
    };

    this.removeTermTaxonomy = function(termTaxonomy){
        var ctrl = this;
        this.termtaxonomyJob = true;
        return this.content.deleteTermTaxonomy(termTaxonomy)
        .then(function(){
            // TODO: maso, 2019: flux fire content termtaxonomy removed
            ctrl.loadContent();
        })
        .finally(function(){
            ctrl.termtaxonomyJob = false;
        });
    };

    this.addMetadata = function(){
        var ctrl = this;
        this.metadataJob = true;
        return $resource.get('/cms/microdata', {
            style:{
                title: 'Term taxonomy',
            },
            data: null
        })
        .then(function(microdata){
            return ctrl.content.putMetadatum(microdata);
        })
        .then(function(microdata){
            // fire new data
            $dispatcher.dispatch('/cms/microdata', {
                type: 'create',
                value: microdata
            });
        }, function(error){
            handleError('Adding Microdatum Fail', 'Fail to add microdatum to the content', error);
        })
        .finally(function(){
            ctrl.metadataJob = false;
        });
    };

    this.deleteMetadata = function (microdata){
        this.metadataJob = true;
        $window.confirm('Delete the microdata')
        .then(function(){
            return microdata.delete();
        })
        .then(function(){
            // fire new data
            $dispatcher.dispatch('/cms/microdata', {
                type: 'delete',
                value: microdata
            });
            $window.toast('Microdata is removed successfully');
        }, function(error){
            handleError('Delete Microdata', 'Fail to delete microdatum', error);
        })
        .finally(function(){
            ctrl.metadataJob = false;
        });
    };
    
    this.updateMetadata = function (microdata){
        this.metadataJob = true;
        return microdata.update()
        .then(function(microNew){
            // fire new data
            $dispatcher.dispatch('/cms/microdata', {
                type: 'update',
                value: microNew
            });
        }, function(error){
            handleError('Update Microdata', 'Fail to update microdatum', error);
        })
        .finally(function(){
            ctrl.metadataJob = false;
        });
    };

    this.setContent = function(content){
        this.content = new CmsContent(content);
    };

    this.setTermTaxonomies = function(termTaxonomies){
        this.termTaxonomies = termTaxonomies;
    };

    this.setMetadata = function(metadata){
        this.metadata = [];
        for(var i = 0; i < metadata.length; i++){
            var item = new CmsContentMetadata(metadata[i]);
            item.content_id = this.content.id;
            this.metadata.push(item);
        }
    }


    function handleError(title, message, error){

    }

    // Load content
    this.loadContent = function(){
        var ctrl = this;
        this.loadingContent = true;
        $cms.getContent($routeParams.contentId, {
            graphql: graphqlQuery,
        })//
        .then(function(content){
            // TAXONOMIES
            ctrl.setContent(content);

            // Term taxonomies
            ctrl.setTermTaxonomies(content.term_taxonomies);
            delete content.term_taxonomies;

            // META
            ctrl.setMetadata(content.metas);
            delete content.metas;
        }, handlError)
        .finally(function(){
            ctrl.loadingContent = false;
        });
    };

    this.uploadFile = function(){
        var ctrl = this;
        ctrl.loading = true;
        $resource.get('local-file', {
            style:{
                accept: '*'
            }
        })
        .then(function (file) {
            return ctrl.content.uploadValue(file);
        })
        .then(function (newContent) {
            // TODO: maso, 2019: add notification
            ctrl.content = newContent;
        })
        .finally(function(){
            delete ctrl.loading;
        });
    };

    this.copyContentToClipboard = function(){
        $clipboard.copyTo(this.content);
        // TODO: maso, 2019: add notify
    };

    var ctrl = this;
    this.contentActions =[{
        title: 'Upload new file',
        icon: 'upload',
        action: function(){
            ctrl.uploadFile();
        }
    },{
        title: 'Copy metadata into clipboard',
        icon: 'copy',
        action: function(){
            ctrl.copyContentToClipboard();
        }
    }];

    this.termtaxonomyActions = [{
        title: 'Add new term-taxonomy',
        icon: 'add',
        action: function(){
            ctrl.addTermTaxonomy();
        }
    }];

    this.metadataActions = [{
        title: 'Add new metadatum',
        icon: 'add',
        action: function(){
            ctrl.addMetadata();
        }
    }];


    function getItemIndex(collection, item){
        for(var i = 0; i < collection.length; i++){
            if(collection[i].id == item.id){
                return i;
            }
        }
        return -1;
    }

    function addToCollection(collection, item){
        var index = getItemIndex(collection, item);
        if(index >= 0){
            collection[index] = item;
        } else {
            collection.push(item);
        }
    }

    function removeFromCollection(collection, item){
        var index = getItemIndex(collection, item);
        if(index >= 0){
            collection.splice(index, 1);
        }
    }

    /*
     * Load the controller
     */
    var microdataCallback = $dispatcher.on('/cms/microdata', function(data) {
        if(data.value.content_id != ctrl.content.id){
            return;
        }
        switch(data.type){
        case 'create':
        case 'update':
            addToCollection(ctrl.metadata, data.value);
            break;
        case 'delete':
            removeFromCollection(ctrl.metadata, data.value);
            break;
        }
    });

    $scope.$on('$destroy', function() {
        $dispatcher.off('/cms/microdata', microdataCallback);
    });

    this.loadContent();
});


'use strict';

angular.module('ngMaterialDashboardCms')

/**
 * @ngdoc function
 * @name ngMaterialDashboard.controller:ContentsCtrl
 * @description # ContentsCtrl Controller of the ngMaterialDashboard
 */
.controller('AmdContentsCtrl', function ($scope, $usr, $controller, $navigator) {
    // Extends with ItemsController
    angular.extend(this, $controller('MbSeenCmsContentsCtrl', {
        $scope: $scope
    }));
    this.addAction({
        title: 'New content',
        icon: 'add',
        action: function () {
            $navigator.openPage('/contents/new');
        }
    });
    this.init();
});

'use strict';

angular.module('ngMaterialDashboardCms')

	/**
	 * @ngdoc Controllers
	 * @name AmdCmsTermTaxonomiesCtrl
	 * @description # Manages Term-Taxonomies
	 */
	.controller('AmdCmsTermTaxonomiesCtrl', function ($scope, $cms, $controller, $navigator) {

	    /*
	     * Extends collection controller
	     */
	    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
		$scope: $scope
	    }));

	    // Override the schema function
	    this.getModelSchema = function () {
		return $cms.termTaxonomySchema();
	    };

	    // get models
	    this.getModels = function (parameterQuery) {
		return $cms.getTermTaxonomies(parameterQuery);
	    };

	    // get a model
	    this.getModel = function (id) {
		return $cms.getTermTaxonomy(id);
	    };

	    // add a model
	    this.addModel = function (model) {
		return $cms.putTermTaxonomy(model);
	    };

	    // delete model
	    this.deleteModel = function (model) {
		return $cms.deleteTermTaxonomy(model.id);
	    };


	    /*
	     * init ctrl
	     */
	    this.init({
		eventType: '/term-taxonomies',
		addAction: {
		    title: 'New term-taxonomy',
		    icon: 'add',
		    dialog: 'views/dialogs/amd-term-taxonomy-new.html'
		},
		deleteAction: {
		    title: 'Delete term-taxonomy?'
		},
		actions: []
	    });
	});

'use strict';

angular.module('ngMaterialDashboardCms')

	/**
	 * @ngdoc Controllers
	 * @name AmdCmsTermTaxonomyNewCtrl
	 * @description Manage process of creation
	 */
	.controller('AmdCmsTermTaxonomyNewCtrl', function ($scope, $cms, $controller, QueryParameter) {

	    /*
	     * Extends collection controller from MbAbstractCtrl 
	     */
	    angular.extend(this, $controller('MbAbstractCtrl', {
		$scope: $scope
	    }));


	    /**
	     * Search for states
	     * 
	     */
	    this.querySearch = function (query) {
		var queryParameter = new QueryParameter();
		queryParameter.setOrder('id', 'd');
		queryParameter.setQuery(query);
		return $cms.getTerms(queryParameter)
			.then(function (pageList) {
			    return pageList.items;
			});
	    };

	    this.setInitialItem = function (termId) {
		var ctrl = this;
		if (termId) {
		    $cms.getTerm(termId)
			    .then(function (term) {
				ctrl.selectedItem = term;
			    });
		}
	    };

	});

'use strict';

angular.module('ngMaterialDashboardCms')

	/**
	 * @ngdoc function
	 * @name ngMaterialDashboardCms.controller:AmdCmsTermTaxonomyCtrl
	 * @description # TaxonomyCtrl Controller of the ngMaterialDashboardCms
	 */
	.controller('AmdCmsTermTaxonomyCtrl', function ($cms, $translate, $routeParams, QueryParameter) {

	    this.loadingTaxonomy = true;
	    this.savingTaxonomy = false;
	    this.edit = false;
	    this.taxonomy = null;


	    /*
	     * Load the term
	     */
	    this.loadTaxonomy = function () {
		var ctrl = this;
		this.loadingTaxonomy = true;
		$cms.getTermTaxonomy($routeParams.taxonomyId)//
			.then(function (taxonomy) {
			    ctrl.taxonomy = taxonomy;
			    ctrl.loadTaxonomies();
			}, function () {
			    alert($translate.instant('Failed to load taxonomy'));
			})
			.finally(function () {
			    ctrl.loadingTaxonomy = false;
			});
	    };


	    /*
	     * Save term
	     */
	    this.save = function () {
		var ctrl = this;
		this.savingTaxonomy = true;
		this.taxonomy.update()//
			.then(function () {
			    ctrl.edit = false;
			}, function () {
			    alert($translate.instant('Failed to update term'));
			})//
			.finally(function () {
			    ctrl.savingTaxonomy = false;
			});
	    };
	    
	    /*
	     * Load available parent term-taxonomies (term-taxonomies which their term_id is the same as term_id of current taxonomy)
	     */
	    this.loadTaxonomies = function () {
		var ctrl = this;
		var pp = new QueryParameter;
		pp.setFilter('term_id',this.taxonomy.term_id);
		$cms.getTermTaxonomies(pp)
			.then(function (res) {
			    ctrl.termTaxonomies = res.items;
			    ctrl.removeCurrentTaxonomy();
			});
	    };
	    
	    /*
	     * Remove ctrl.taxonomy from the list of term-taxonomies
	     */
	    this.removeCurrentTaxonomy = function () {
		for (var i = 0; i < this.termTaxonomies.length ; i++) {
		    if (this.termTaxonomies[i].id === this.taxonomy.id) {
			this.termTaxonomies.splice(i,1);
			break;
		    }
		}
	    };

	    this.loadTaxonomy();
	});


'use strict';

angular.module('ngMaterialDashboardCms')

	/**
	 * @ngdoc function
	 * @name ngMaterialDashboardCms.controller:AmdCmsTermCtrl
	 * @description # TermCtrl Controller of the ngMaterialDashboardCms
	 */
	.controller('AmdCmsTermCtrl', function ($navigator, $cms, $translate, $routeParams, $location, QueryParameter) {

	    this.loadingTerm = true;
	    this.savingTerm = false;
	    this.edit = false;
	    this.term = null;


	    function handlError() {
		alert($translate.instant('Failed to load items'));
	    }

	    /*
	     * Load the term
	     */
	    this.loadTerm = function () {
		var ctrl = this;
		this.loadingTerm = true;
		$cms.getTerm($routeParams.termId)//
			.then(function (term) {
			    ctrl.term = term;
			    ctrl.loadMetas();
			    ctrl.loadTaxonomies();
			}, function () {
			    alert($translate.instant('Failed to load term'));
			})
			.finally(function () {
			    ctrl.loadingTerm = false;
			});
	    };

	    /*
	     * Load metas of term
	     */
	    this.loadMetas = function () {
		var ctrl = this;
		this.loadingMetas = true;
		ctrl.term.getMetadatas()//
			.then(function (res) {
			    ctrl.term.metas = res.items;
			}, handlError)
			.finally(function () {
			    ctrl.loadingMetas = false;
			});
	    };

	    /*
	     * Load taxonomies of term
	     */
	    this.loadTaxonomies = function () {
		var pp = new QueryParameter;
		pp.setFilter('term_id', this.term.id);
		var ctrl = this;
		this.loadingTaxonomies = true;
		//TODO: Masood,2019: Change the $cms.getTermTaxonomies(pp) to ctrl.term.getTermTaxonomies()
		$cms.getTermTaxonomies(pp)
			.then(function (res) {
			    ctrl.term.taxonomies = res.items || {};
			}, function () {
			    alert($translate.instant('Failed to load taxonomies'));
			})
			.finally(function () {
			    ctrl.loadingTaxonomies = false;
			});
	    };

	    /*
	     * Remove term
	     */
	    this.remove = function () {
		var ctrl = this;
		confirm('delete term ' + this.term.id + '?')//
			.then(function () {
			    ctrl.removingTerm = true;
			    return ctrl.term.delete();//
			})//
			.then(function () {
			    $location.path('terms');
			}, function (error) {
			    alert($translate.instant('Failed to delete term') + error.message);
			})//
			.finally(function () {
			    ctrl.removingTerm = false;
			});
	    };

	    /*
	     * Save term
	     */
	    this.save = function () {
		var ctrl = this;
		this.savingTerm = true;
		this.term.update()//
			.then(function () {
			    ctrl.edit = false;
			}, function () {
			    alert($translate.instant('Failed to update term'));
			})//
			.finally(function () {
			    ctrl.savingTerm = false;
			});;
	    };

	    /*
	     * Edit a Meta of the term
	     */
	    this.editMeta = function (meta, index) {
		var ctrl = this;
		$navigator.openDialog({
		    templateUrl: 'views/dialogs/amd-meta.html',
		    config: {
			model: angular.copy(meta)
		    }
		})//
			.then(function (meta) {
			    ctrl.updatingMeta = true;
			    return meta.update();
			})//
			.then(function (newMeta) {
			    ctrl.term.metas[index] = newMeta;
			})//
			.finally(function () {
			    ctrl.updatingMeta = false;
			});
	    };

	    /*
	     * Remove a Meta of the term
	     */
	    this.removeMeta = function (meta, index) {
		var ctrl = this;
		confirm('delete meta ' + meta.id + '?')//
			.then(function () {
			    ctrl.removingMeta = true;
			    return meta.delete();//
			})//
			.then(function () {
			    ctrl.term.metas.splice(index, 1);
			}, function (error) {
			    alert($translate.instant('Failed to delete meta') + error.message);
			})//
			.finally(function () {
			    ctrl.removingMeta = false;
			});
	    };

	    /*
	     * Add meta to term
	     */
	    var ctrl = this;
	    function addMeta() {
		$navigator.openDialog({
		    templateUrl: 'views/dialogs/amd-meta.html',
		    config: {
			model: {}
		    }
		})//
			.then(function (meta) {
			    ctrl.addingMeta = true;
			    return ctrl.term.putMetadatum(meta);
			})//
			.then(function (meta) {
			    ctrl.term.metas = ctrl.term.metas.concat(meta);
			})//
			.finally(function () {
			    ctrl.addingMeta = false;
			});
	    }


	    /*
	     * Edit a taxonomy of the term
	     */
	    this.editTaxonomy = function (taxonomy, index) {
		var ctrl = this;
		$navigator.openDialog({
		    templateUrl: 'views/dialogs/amd-in-term-taxonomy-new.html',
		    config: {
			model: angular.copy(taxonomy)
		    }
		})//
			.then(function (taxonomy) {
			    ctrl.updatingTaxonomy = true;
			    return taxonomy.update();
			})//
			.then(function (newTaxonomy) {
			    ctrl.term.taxonomies[index] = newTaxonomy;
			})//
			.finally(function () {
			    ctrl.updatingTaxonomy = false;
			});
	    };

	    /*
	     * Remove a taxonomy of the term
	     */
	    this.removeTaxonomy = function (taxonomy, index) {
		var ctrl = this;
		confirm('delete taxonomy ' + taxonomy.id + '?')//
			.then(function () {
			    ctrl.removingTaxonomy = true;
			    return taxonomy.delete();//
			})//
			.then(function () {
			    ctrl.term.taxonomies.splice(index, 1);
			}, function (error) {
			    alert($translate.instant('Failed to delete taxonomy') + error.message);
			})//
			.finally(function () {
			    ctrl.removingTaxonomy = false;
			});
	    };

	    /*
	     * Add taxonomy to the term
	     */
	    function addTaxonomy() {
		$navigator.openDialog({
		    templateUrl: 'views/dialogs/amd-in-term-taxonomy-new.html',
		    config: {
			model: {}
		    }
		})//
			.then(function (taxonomy) {
			    ctrl.addingTaxonomy = true;
			    taxonomy.term_id = ctrl.term.id;
			    //TODO: Masood, 2019: Replace the line with: return ctrl.term.putTermTaxonomy(taxonomy)
			    return $cms.putTermTaxonomy(taxonomy);
			})//
			.then(function (taxonomy) {
			    ctrl.term.taxonomies = ctrl.term.taxonomies.concat(taxonomy);
			})//
			.finally(function () {
			    ctrl.addingTaxonomy = false;
			});
	    }

	    this.metaActions = [{
		    title: 'New meta',
		    icon: 'add',
		    action: addMeta
		}];

	    this.taxonomyActions = [{
		    title: 'New taxonomy',
		    icon: 'add',
		    action: addTaxonomy
		}];

	    this.loadTerm();
	});


'use strict';

angular.module('ngMaterialDashboardCms')

/**
 * @ngdoc Controllers
 * @name AmdCmsTermsCtrl
 * @description # Manages Terms
 */
.controller('AmdCmsTermsCtrl', function ($scope, $cms, $controller, $navigator) {

    /*
     * Extends collection controller
     */
    angular.extend(this, $controller('MbSeenAbstractCollectionCtrl',{
        $scope : $scope
    }));

    // Override the schema function
    this.getModelSchema = function () {
        return $cms.termSchema();
    };

    // get contents
    this.getModels = function (parameterQuery) {
        return $cms.getTerms(parameterQuery);
    };

    // get a content
    this.getModel = function (id) {
        return $cms.getTerm(id);
    };

    // delete account
    this.deleteModel = function (model) {
	var ctrl = this;
        return $cms.deleteTerm(model.id)
		.then(function () {
		    ctrl.reload();
		});
    };
    
    // adding new term
    this.addModel = function (model) {
        return $cms.putTerm(model);
    };

    this.init({
        // dispatcher path and internal address
        eventType: '/contents',
        
        // add creation actions
        addAction: {
            title: 'New term',
            icon: 'add',
            dialog: 'views/dialogs/amd-term-new.html'
        },
        // delete action
        deleteAction: {
            title: 'Delete term?'
        },
        // list of actions in the view
        actions: []
    });
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboard')

/**
 * @ngdoc directive
 * @name wbInfinateScroll
 * @description # wbInfinateScroll
 */
.directive('amdContentIcon', function($q, $parse) {

	/**
	 * Link data and view
	 */
	function postLink(scope, attr, elem, ngModel) {
		/*
		 * Load types
		 */
		ngModel.$render = function(){
			scope.content = ngModel.$viewValue;
			if(!scope.content){
				scope.type = 'insert_drive_file';
				return;
			}
			if(angular.isArray(scope.content.mime_type.match(/.*(ogg|mp3|mpeg).*/i))){
				scope.icon = 'audiotrack';
			} else if(angular.isArray(scope.content.mime_type.match(/.*(video).*/i))){
				scope.icon = 'video_library';
			} else if(angular.isArray(scope.content.mime_type.match(/.*(image).*/i))){
				scope.icon = 'image';
			} else if(angular.isArray(scope.content.mime_type.match(/.*(weburger).*/i))){
				scope.icon = 'chrome_reader_mode';
			} else {
				scope.icon = 'insert_drive_file';
			}
		};
	}
	
	return {
		restrict : 'E',
		transclude : false,
		replace: true,
		require: 'ngModel',
		template : '<ng-md-icon icon="{{icon}}"></ng-md-icon>',
		link: postLink
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboard')

/**
 * @ngdoc directive
 * @name wbInfinateScroll
 * @description # wbInfinateScroll
 */
.directive('amdContentPreview', function($q, $parse) {

	/**
	 * Link data and view
	 */
	function postLink(scope, attr, elem, ngModel) {
		
		function loadContent(content){
			if(scope.loadingModel ){
				return;
			}
			scope.loadingModel = true;
			return content.downloadValue()
			.then(function(model){
				scope.model = model;
			})
			.finally(function(){
				scope.loadingModel = false;
			});
		}
		/*
		 * Load types
		 */
		ngModel.$render = function(){
			scope.content = ngModel.$viewValue;
			if(!scope.content){
				scope.type = 'unknown';
				return;
			}
			if(angular.isArray(scope.content.mime_type.match(/.*(ogg|mp3|mpeg).*/i))){
				scope.type = 'audio';
			} else if(angular.isArray(scope.content.mime_type.match(/.*(video).*/i))){
				scope.type = 'video';
			} else if(angular.isArray(scope.content.mime_type.match(/.*(image).*/i))){
				scope.type = 'image';
			} else if(angular.isArray(scope.content.mime_type.match(/.*(weburger).*/i))){
				scope.type = 'weburger';
				loadContent(scope.content);
			} else {
				scope.type = 'unknown';
			}
		};
	}
	
	return {
		restrict : 'E',
		transclude : false,
		replace: true,
		require: 'ngModel',
		templateUrl : 'views/directives/amd-content-preview.html',
		link: postLink
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardCms')
/**
 * دریچه‌های محاوره‌ای
 */
.run(function($navigator) {
	$navigator.newGroup({
		id: 'content-management',
		title: 'Content management',
		description: 'Manage all content of the system.',
		icon: 'image',
		hidden: '!app.user.tenant_owner',
		priority: 5
	});
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardCms').run(function ($resource, CmsTermTaxonomy) {
    $resource.newPage({
        type: 'cms.term-taxonomies.single',
        tags: ['cms/term-taxonomies'],
        label: 'Content term-taxonomy',
        icon: 'label',
        templateUrl: 'views/resources/amd-term-taxonomy.html',
        /*
         * @ngInject
         */
        controller: function ($scope) {
            $scope.multi = false;
            this.value = $scope.value;
            this.setSelected = function (item) {
                $scope.$parent.setValue(new CmsTermTaxonomy(item));
                $scope.$parent.answer();
            };
            this.isSelected = function (item) {
                return item === this.value || item.id === this.value.id;
            };
        },
        controllerAs: 'resourceCtrl',
        priority: 8
    });
    
    
    $resource.newPage({
        type: 'cms.microdata.keyval',
        tags: ['/cms/microdata'],
        label: 'Microdatum',
        icon: 'label',
        templateUrl: 'views/resources/amd-cms-microdatum.html',
        priority: 8
    });
});
angular.module('ngMaterialDashboardCms').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-content-new.html',
    "<md-content class=md-padding layout-padding flex> <form name=contentForm ng-action=add(config) mb-preloading=ctrl.savingContent layout-margin layout=column flex> <md-input-container> <label>Name</label> <input name=name ng-model=config.model.name required> <div ng-messages=contentForm.name.$error> <div ng-message=required>This is required!</div> </div> </md-input-container> <md-input-container> <label>Description</label> <input ng-model=config.model.description> </md-input-container> <md-input-container> <label>Title</label> <input ng-model=config.model.title> </md-input-container> <md-input-container> <lf-ng-md-file-input lf-files=config.files progress preview drag required></lf-ng-md-file-input> <div ng-messages=!lf-files.file[0]> <div ng-message=required>This is required!</div> </div> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=add(config)> <wb-icon aria-label=close>done</wb-icon> <span translate>Add</span> </md-button> <md-button class=md-raised ng-click=cancel()> <wb-icon aria-label=close>close</wb-icon> <span translate>Cancel</span> </md-button> </div> </form> </md-content>"
  );


  $templateCache.put('views/amd-content.html',
    "<md-content flex> <mb-titled-block mb-title=Information mb-more-actions=ctrl.contentActions mb-progress=ctrl.loading layout=column> <div layout=row> <amd-content-preview ng-model=ctrl.content md-margin> </amd-content-preview> <table md-margin> <tr> <td translate>ID</td> <td>{{::ctrl.content.id}}</td> </tr> <tr> <td translate>Name</td> <td> <mb-inline ng-model=ctrl.content.name mb-inline-type=text mb-inline-label=Name mb-inline-enable=true mb-inline-on-save=ctrl.save()>{{ctrl.content.name || '...'}}</mb-inline> </td> </tr> <tr> <td translate>Title</td> <td> <mb-inline ng-model=ctrl.content.title mb-inline-type=text mb-inline-label=Title mb-inline-enable=true mb-inline-on-save=ctrl.save()> {{ctrl.content.title || '...'}} </mb-inline> </td> </tr> <tr> <td translate>Description</td> <td> <mb-inline ng-model=ctrl.content.description mb-inline-type=textarea mb-inline-label=Description mb-inline-enable=true mb-inline-on-save=ctrl.save()> {{ctrl.content.description || '...'}} </mb-inline> </td> </tr> <tr> <td translate>File name</td> <td> <mb-inline ng-model=ctrl.content.file_name mb-inline-type=text mb-inline-label=\"File name\" mb-inline-enable=true mb-inline-on-save=ctrl.save()> {{ctrl.content.file_name || '...'}} </mb-inline> </td> </tr> <tr> <td translate>File size</td> <td>{{ctrl.content.file_size}}</td> </tr> <tr> <td translate>Media type</td> <td> <mb-inline ng-model=ctrl.content.media_type mb-inline-type=text mb-inline-label=\"Media type\" mb-inline-enable=true mb-inline-on-save=ctrl.save()> {{ctrl.content.media_type || '...'}} </mb-inline> </td> </tr> <tr> <td translate>Mimetype</td> <td> <mb-inline ng-model=ctrl.content.mime_type mb-inline-type=text mb-inline-label=Mimetype mb-inline-enable=true mb-inline-on-save=ctrl.save()> {{ctrl.content.mime_type || '...'}} </mb-inline> </td> </tr> <tr> <td translate>Downloads count</td> <td>{{::ctrl.content.downloads}}</td> </tr> <tr> <td translate>Creation Date</td> <td>{{ctrl.content.creation_dtime | mbDate}}</td> </tr> <tr> <td translate>Last Modification Date</td> <td>{{ctrl.content.modif_dtime | mbDate}}</td> </tr> </table> </div>  <div layout=row ng-show=!ctrl.edit> <span flex></span> <md-button ng-href=/api/v2/cms/contents/{{ctrl.content.id}}/content download=\"{{ctrl.content.file_name || ctrl.content.name}}\" target=_blank class=\"md-raised md-primary\"> <span translate>Download</span> </md-button> <md-button class=\"md-raised md-accent\" ng-click=ctrl.remove()> <span translate>Delete</span> </md-button> </div> </mb-titled-block>  <mb-titled-block mb-progress=ctrl.termtaxonomyJob mb-more-actions=ctrl.termtaxonomyActions mb-title=\"Term Taxonomies\"> <md-chips ng-model=ctrl.termTaxonomies name=termTaxonomies readonly md-removable=true md-on-remove=ctrl.removeTermTaxonomy($chip) md-max-chips=5 placeholder=\"Enter a term\" input-aria-label=Terms> <md-chip-template> <strong>{{::$chip.term.name}}</strong> <em>({{::$chip.taxonomy}})</em> </md-chip-template> </md-chips> </mb-titled-block>  <mb-titled-block mb-title=Metadata mb-more-actions=ctrl.metadataActions mb-progress=ctrl.metadataJob layout=column> <md-input-container ng-repeat=\"m in ctrl.metadata\" class=\"md-icon-float md-icon-right md-block\"> <label>{{::m.key}}</label> <wb-icon ng-if=ctrl.canEditMetadata(m)>edit</wb-icon> <input ng-model=m.value ng-change=ctrl.updateMetadata(m) ng-model-options=\"{updateOn:'default change blur',debounce:{default:1000,blur:0,change:0}}\"> <wb-icon ng-click=ctrl.deleteMetadata(m)>delete</wb-icon> </md-input-container> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-contents.html',
    "<div ng-init=\"ctrl.setDataQuery('{id, title, name, file_name, description, mime_type, media_type, status, author{id,profiles{first_name,last_name}}}')\" mb-preloading=ctrl.loading layout=column flex>  <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"content in ctrl.items track by content.id\" ng-href=content/{{::content.id}} class=md-3-line> <amd-content-icon ng-model=content> </amd-content-icon> <div class=md-list-item-text layout=column> <h3>{{::content.title}} (<span translate=\"\">Author</span> : {{::content.author.profiles[0].first_name}} {{::content.author.profiles[0].last_name}})</h3> <h4>{{::content.name}}</h4> <p>{{::content.description}}</p> </div> <md-button ng-href=/api/v2/cms/contents/{{content.id}}/content download=\"{{content.file_name || content.name}}\" target=_blank class=\"md-secondary md-icon-button\"> <wb-icon aria-label=download>download </wb-icon></md-button> <wb-icon class=md-secondary ng-click=\"ctrl.deleteItem(content, $event)\" aria-label=Delete>delete </wb-icon></md-list-item> </md-list> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.state === 'ideal' && (!ctrl.items || ctrl.items.length == 0)\"> <h2 translate=\"\">No item found</h2> </div> </md-content> </div>"
  );


  $templateCache.put('views/amd-term-taxonomies.html',
    "<div ng-init=\"ctrl.setDataQuery('{id, taxonomy, description, count, term{id, name}}')\" mb-preloading=ctrl.loading layout=column flex>     <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <md-list flex> <md-list-item ng-repeat=\"content in ctrl.items track by content.id\" ng-href=term-taxonomies/{{::content.id}} class=md-3-line> <div class=md-list-item-text layout=column> <h3>{{::content.term.name}} ({{::content.count}})</h3> <h4>{{::content.taxonomy}}</h4> <p>{{::content.description}}</p> </div> <wb-icon class=md-secondary ng-click=\"ctrl.deleteItem(content, $event)\" aria-label=Delete> delete </wb-icon> </md-list-item> </md-list> </md-content> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.state === 'ideal' && (!ctrl.items || ctrl.items.length == 0)\"> <h2 translate=\"\">No item found</h2> </div> </div>"
  );


  $templateCache.put('views/amd-term-taxonomy.html',
    "<md-content flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loadingTaxonomy || ctrl.savingTaxonomy || ctrl.removingTaxonomy\" mb-title=\"{{'Taxonomy'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.taxonomy.id}}</td> </tr> <tr> <td translate=\"\">Taxonomy </td> <td>: {{ctrl.taxonomy.taxonomy}}</td> </tr> <tr> <td translate=\"\">Description </td> <td>: {{ctrl.taxonomy.description}}</td> </tr> <tr> <td translate=\"\">Parent id </td> <td>: {{ctrl.taxonomy.parent_id}}</td> </tr> <tr> <td translate=\"\">Term id</td> <td>: {{ctrl.taxonomy.term_id}}</td> </tr> </table> </div> <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.remove()> <span translate=\"\">Delete</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=\"ctrl.edit = true;\"> <span translate=\"\">Edit</span> </md-button> </div> </mb-titled-block>  <mb-titled-block ng-if=ctrl.edit mb-progress=ctrl.savingTaxonomy mb-title=\"{{'Edit taxonomy'| translate}}\" layout=column> <form name=myForm ng-action=ctrl.save() layout=column flex> <md-input-container> <label translate=\"\">Taxonomy</label> <input ng-model=ctrl.taxonomy.taxonomy name=taxonomy required> <div ng-messages=myForm.taxonomy.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <div ng-controller=\"AmdCmsTermTaxonomyNewCtrl as termTaxctrl\" ng-init=termTaxctrl.setInitialItem(ctrl.taxonomy.term_id)> <md-autocomplete flex required md-clear-button=true md-input-name=termId md-selected-item=termTaxctrl.selectedItem md-selected-item-change=\"ctrl.taxonomy.term_id = termTaxctrl.selectedItem.id\" md-search-text=termTaxctrl.searchText md-items=\"item in termTaxctrl.querySearch(termTaxctrl.searchText)\" md-item-text=item.name md-require-match md-floating-label=\"{{'Term'| translate}}\"> <md-item-template> <span md-highlight-text=termTaxctrl.searchText>{{item.name}}</span> </md-item-template> </md-autocomplete> </div> <md-input-container> <label translate=\"\">Parent</label> <md-select ng-model=ctrl.taxonomy.parent_id> <md-option><em>None</em></md-option> <md-option ng-repeat=\"tax in ctrl.termTaxonomies\" ng-value=tax.id> {{tax.taxonomy}} </md-option> </md-select> </md-input-container> <md-input-container> <label translate>Description</label> <textarea ng-model=ctrl.taxonomy.description></textarea> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=\"ctrl.edit = false\"> <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-disabled=myForm.$invalid ng-click=ctrl.save()> <span translate=\"\">Save</span> </md-button> </div> </form> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-term.html',
    "<md-content flex> <mb-titled-block ng-show=!ctrl.edit mb-progress=\"ctrl.loadingTerm || ctrl.savingTerm || ctrl.removingTerm\" mb-title=\"{{'Term'| translate}}\" layout=column> <div layout=row layout-xs=column layout-align-xs=\"center center\" layout-padding> <table> <tr> <td translate=\"\">ID </td> <td>: {{ctrl.term.id}}</td> </tr> <tr> <td translate=\"\">Name </td> <td>: {{ctrl.term.name}}</td> </tr> <tr> <td translate=\"\">Slug </td> <td>: {{ctrl.term.slug}}</td> </tr> </table> </div> <div ng-show=!ctrl.edit layout-gt-xs=row layout=column> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=ctrl.remove()> <span translate=\"\">Delete</span> </md-button> <md-button class=\"md-raised md-primary\" ng-click=\"ctrl.edit = true;\"> <span translate=\"\">Edit</span> </md-button> </div> </mb-titled-block>  <mb-titled-block ng-show=ctrl.edit mb-progress=ctrl.savingTerm mb-title=\"{{'Edit term'| translate}}\" layout=column> <form name=myForm ng-action=ctrl.save() layout=column flex> <md-input-container> <label translate=\"\">Name</label> <input ng-model=ctrl.term.name name=name required> <div ng-messages=myForm.name.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <md-input-container> <label translate=\"\">Slug</label> <input ng-model=ctrl.term.slug name=slug required> <div ng-messages=myForm.slug.$error> <div ng-message=required translate=\"\">This field is required.</div> </div> </md-input-container> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=\"ctrl.edit = false\"> <span translate=\"\">Cancel</span> </md-button> <md-button class=\"md-raised md-primary\" ng-disabled=myForm.$invalid ng-click=ctrl.save()> <span translate=\"\">Save</span> </md-button> </div> </form> </mb-titled-block>  <mb-titled-block mb-progress=\"ctrl.loadingMetas || ctrl.updatingMeta || ctrl.addingMeta || ctrl.removingMeta\" mb-title=\"{{'Metas'| translate}}\" layout=column mb-more-actions=ctrl.metaActions> <div layout=column ng-if=\"ctrl.metas.length == 0\"> <p style=\"text-align: center\" translate=\"\">List is empty.</p> </div> <md-list flex> <md-list-item ng-repeat=\"meta in ctrl.term.metas track by meta.id\" class=md-2-line> <div class=md-list-item-text layout=column> <h3><span translate=\"\">Key</span>: {{meta.key}}</h3> <h3><span translate=\"\">Value</span>: {{meta.value}}</h3> </div> <wb-icon class=md-secondary ng-click=\"ctrl.removeMeta(meta, $index)\" aria-label=Delete> delete </wb-icon> <wb-icon class=md-secondary ng-click=\"ctrl.editMeta(meta, $index)\" aria-label=Delete> edit </wb-icon> <md-divider></md-divider> </md-list-item> </md-list> </mb-titled-block>  <mb-titled-block mb-progress=\"ctrl.loadingTaxonomies || ctrl.updatingTaxonomy || ctrl.addingTaxonomy || removingTaxonomy\" mb-title=\"{{'Taxonomies'| translate}}\" layout=column mb-more-actions=ctrl.taxonomyActions> <div layout=column ng-if=\"ctrl.term.taxonomies.length == 0\"> <p style=\"text-align: center\" translate=\"\">List is empty.</p> </div> <md-list flex> <md-list-item ng-repeat=\"tax in ctrl.term.taxonomies track by tax.id\" ng-href=term-taxonomies/{{tax.id}} class=md-2-line> <div class=md-list-item-text layout=column> <h3><span translate=\"\">Taxonomy</span>: {{tax.taxonomy}}</h3> <h3><span translate=\"\">Description</span>: {{tax.description}}</h3> </div> <wb-icon class=md-secondary ng-click=\"ctrl.removeTaxonomy(tax, $index)\" aria-label=Delete> delete </wb-icon> <wb-icon class=md-secondary ng-click=\"ctrl.editTaxonomy(tax, $index)\" aria-label=Delete> edit </wb-icon> <md-divider></md-divider> </md-list-item> </md-list> </mb-titled-block> </md-content>"
  );


  $templateCache.put('views/amd-terms.html',
    "<div ng-init=\"ctrl.setDataQuery('{id, name, slug}')\" mb-preloading=\"ctrl.state=='busy'\" layout=column flex>     <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions()> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex>  <md-list flex> <md-list-item ng-repeat=\"term in ctrl.items track by term.id\" ng-href=terms/{{::term.id}}> <p>{{::term.name}}</p> <wb-icon class=md-secondary ng-click=\"ctrl.deleteItem(term, $event)\" aria-label=Delete> delete </wb-icon> </md-list-item> </md-list> </md-content> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.state === 'ideal' && (!ctrl.items || ctrl.items.length == 0)\"> <h2 translate=\"\">No item found</h2> </div> </div>"
  );


  $templateCache.put('views/dialogs/amd-in-term-taxonomy-new.html',
    "<md-dialog ng-cloak> <form name=myForm flex layout=column ng-action=answer(config.model)> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>New term-taxonomy</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.model) aria-label=close> <wb-icon>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel() aria-label=close> <wb-icon>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding style=\"padding-top: 16px\"> <md-input-container> <label translate>Taxonomy name</label> <input ng-model=config.model.taxonomy> </md-input-container> <md-input-container> <label translate>Description</label> <textarea ng-model=config.model.description></textarea> </md-input-container> </md-dialog-content> </form> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-meta.html',
    "<md-dialog ng-cloak> <form name=myForm flex layout=column ng-action=answer(config.model)> <md-toolbar> <div class=md-toolbar-tools> <h2 translate=\"\">Meta</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.model) aria-label=close> <wb-icon>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel() aria-label=close> <wb-icon>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding style=\"padding-top: 16px\"> <md-input-container> <label translate=\"\">Key</label> <input ng-model=config.model.key> </md-input-container> <md-input-container> <label translate=\"\">Value</label> <input ng-model=config.model.value> </md-input-container> </md-dialog-content> </form> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-term-new.html',
    "<md-dialog ng-cloak> <form name=myForm flex layout=column ng-action=answer(config.model)> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>New term</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.model) aria-label=close> <wb-icon>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel() aria-label=close> <wb-icon>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding style=\"padding-top: 16px\"> <md-input-container> <label translate>Name</label> <input ng-model=config.model.name> </md-input-container> <md-input-container> <label translate>Slug</label> <input ng-model=config.model.slug> </md-input-container> </md-dialog-content> </form> </md-dialog>"
  );


  $templateCache.put('views/dialogs/amd-term-taxonomy-new.html',
    "<md-dialog ng-cloak> <form name=myForm flex layout=column ng-action=answer(config.model)> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>New term-taxonomy</h2> <span flex></span> <md-button class=md-icon-button ng-click=answer(config.model) aria-label=close> <wb-icon>done</wb-icon> </md-button> <md-button class=md-icon-button ng-click=cancel() aria-label=close> <wb-icon>close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content layout=column layout-padding ng-controller=\"AmdCmsTermTaxonomyNewCtrl as ctrl\" style=\"padding-top: 16px\"> <md-input-container> <label translate>Taxonomy</label> <input ng-model=config.model.taxonomy> </md-input-container>     <md-autocomplete flex required md-clear-button=true md-input-name=termId md-selected-item=ctrl.selectedItem md-selected-item-change=\"config.model.term_id = ctrl.selectedItem.id\" md-search-text=ctrl.searchText md-items=\"item in ctrl.querySearch(ctrl.searchText)\" md-item-text=item.name md-require-match md-floating-label=\"{{'Term'| translate}}\"> <md-item-template> <span md-highlight-text=ctrl.searchText>{{item.name}}</span> </md-item-template> </md-autocomplete> <md-input-container> <label translate>Description</label> <textarea ng-model=config.model.description></textarea> </md-input-container> </md-dialog-content> </form> </md-dialog>"
  );


  $templateCache.put('views/directives/amd-content-preview.html',
    "<div style=text-align:center md-colors=\"{borderColor: 'primary-200'}\"> <img class=amd-content-preview style=\"border: solid 1px;border-color:inherit\" ng-if=\"type === 'image'\" ng-src=/api/v2/cms/contents/{{content.id}}/content alt={{amdContent.title}}> <audio class=amd-content-preview style=\"border: solid 1px;border-color:inherit\" ng-if=\"type === 'audio'\" controls> <source src=/api/v2/cms/contents/{{content.id}}/content type={{content.mime_type}}> Your browser does not support the audio tag. </source></audio> <video class=amd-content-preview style=\"border: solid 1px;border-color:inherit\" ng-if=\"type === 'video'\" controls> <source src=/api/v2/cms/contents/{{content.id}}/content type={{content.mime_type}}> Your browser does not support the video tag. </source></video>  <md-content ng-if=\"type === 'weburger'\" style=\"max-height: 400px\"> <wb-group class=amd-content-preview wb-editable=editable ng-model=model> </wb-group> </md-content> </div>"
  );


  $templateCache.put('views/resources/amd-cms-microdatum.html',
    "<div layout=column style=\"padding: 16px\" flex> <md-input-container> <label translate=\"\">Key</label> <input ng-model=value.key> </md-input-container> <md-input-container> <label translate=\"\">Value</label> <input ng-model=value.value> </md-input-container> </div>"
  );


  $templateCache.put('views/resources/amd-term-taxonomy.html',
    "<div ng-controller=\"AmdCmsTermTaxonomiesCtrl as ctrl\" ng-init=\"ctrl.setDataQuery('{id, taxonomy, description, count, term{id, name}}')\" mb-preloading=\"ctrl.state === 'busy'\" layout=column flex> <mb-pagination-bar mb-model=ctrl.queryParameter mb-reload=ctrl.reload() mb-sort-keys=ctrl.getSortKeys() mb-more-actions=ctrl.getActions() mb-properties=ctrl.properties> </mb-pagination-bar> <md-content mb-infinate-scroll=ctrl.loadNextPage() layout=column flex> <md-list flex> <md-list-item ng-repeat=\"content in ctrl.items track by content.id\" ng-click=\"multi || resourceCtrl.setSelected(content)\" class=md-3-line> <div class=md-list-item-text layout=column> <h3>{{::content.term.name}} ({{::content.count}})</h3> <h4>{{::content.taxonomy}}</h4> <p>{{::content.description}}</p> </div> <md-checkbox ng-if=multi class=md-secondary ng-init=\"content.selected = resourceCtrl.isSelected(content)\" ng-model=content.selected ng-change=\"resourceCtrl.setSelected(content, content.selected)\"> </md-checkbox> <md-divider md-inset></md-divider> </md-list-item> </md-list> </md-content> </div>"
  );

}]);
